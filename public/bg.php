<?php
// List files
$files = glob(__DIR__ . '/wallpapers/*.*');

// Get random for the day
srand(date('ymdh'));
$file = $files[rand(0, count($files) - 1)];

// Give filename if ?file is set
if (isset($_GET['file'])) {
    echo str_replace(__DIR__, '', $file);
    exit(0);
}

// Set mime
header('Content-Type: ' . mime_content_type($file));

// Return file
echo file_get_contents($file);
