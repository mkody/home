# Home

## Prerequisite
- Somewhere to host your page
- A web server with PHP enabled
- `git`, `nodejs` and `yarn`
- Wallpapers

## Install
- `git clone https://gitlab.com/mkody/home.git && cd home`
- add wallpapers in `public/wallpapers/`
- `yarn && yarn build`
- serve the `dist/` folder  
  (or move the content of that folder to your public site)

## Q&A
**Q: Why did you use Vue for this??**  
A: Shut up and load my 100KB code. (Because SCSS and functions are a bit easier.)

**Q: Why the PHP file?**  
A: To load a random image for the day from the `public/wallpapers/` folder of course. My web server runs with PHP already, so why not put a nice li'l script in here.

**Q: Can I get a list of all your wallpapers?**  
A: Nope.

## TODO
- [ ] Have things not hardcoded and not public (like the locations for the weather or transports, aniSched/RSS source...)
- [ ] Cache external calls in case they go down and to limit how fast we reach them
- [ ] Include external PHP scripts I use (for anime times [take a look here](https://github.com/mkody/aniSched))
- [x] Upgrade to VueJS 3 (+ Vite)
- [x] Move quotes to an external service ([quotes.kody.workers.dev](https://quotes.kody.workers.dev))

## Screenshots
With unread news:  
![RIP Japan Expo](./.gitlab/screenshot-with-feed.jpg)

When the feed is empty, an anime quote is shown:  
![I tried to put nice quotes in there](./.gitlab/screenshot-feed-allread.png)

## Recommended IDE Setup
- [VS Code](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar)

## Credits
- Weather Icons by [Erik Flowers](http://www.helloerik.com/)
- Weather data from [prevision-meteo.ch](https://prevision-meteo.ch/)
- Transport data from the [Swiss public transport API](http://transport.opendata.ch/)

## License
MIT
