export const weatherIconMapping = {
  'averses-de-neige-faible': 'wi-day-snow',
  'averses-de-pluie-faible': 'wi-day-rain-wind',
  'averses-de-pluie-forte': 'wi-day-rain-wind',
  'averses-de-pluie-moderee': 'wi-day-showers',
  brouillard: 'wi-windy',
  'ciel-voile': 'wi-day-light-wind',
  'couvert-avec-averses': 'wi-showers',
  'developpement-nuageux': 'wi-day-sunny-overcast',
  eclaircies: 'wi-day-cloudy',
  ensoleille: 'wi-day-sunny',
  'faiblement-nuageux': 'wi-cloud',
  'faiblement-orageux': 'wi-day-storm-showers',
  'faibles-passages-nuageux': 'wi-day-sunny-overcast',
  'fortement-nuageux': 'wi-cloudy',
  'fortement-orageux': 'wi-thunderstorm',
  'neige-faible': 'wi-day-snow',
  'neige-forte': 'wi-day-snow-wind',
  'neige-moderee': 'wi-day-snow',
  'nuit-avec-averses': 'wi-night-alt-showers',
  'nuit-avec-averses-de-neige-faible': 'wi-night-alt-snow',
  'nuit-avec-developpement-nuageux': 'wi-night-alt-partly-cloudy',
  'nuit-bien-degagee': 'wi-night-alt-cloudy-high',
  'nuit-claire': 'wi-night-clear',
  'nuit-claire-et-stratus': 'wi-night-alt-cloudy-high',
  'nuit-legerement-voilee': 'wi-night-clear',
  'nuit-nuageuse': 'wi-night-alt-cloudy',
  'orage-modere': 'wi-storm-showers',
  'pluie-et-neige-melee': 'wi-sleet',
  'pluie-et-neige-melee-faible': 'wi-sleet',
  'pluie-et-neige-melee-forte': 'wi-rain-mix',
  'pluie-et-neige-melee-moderee': 'wi-rain-mix',
  'pluie-faible': 'wi-showers',
  'pluie-forte': 'wi-rain',
  'pluie-moderee': 'wi-showers',
  stratus: 'wi-windy',
  'stratus-se-dissipant': 'wi-day-light-wind'
}
